package com.univ;

import javax.persistence.Entity;

@Entity
public class Car extends Vehicule{
	private int numberOFSeats;

	public int getNumberOFSeats() {
		return numberOFSeats;
	}

	public void setNumberOFSeats(int numberOFSeats) {
		this.numberOFSeats = numberOFSeats;
	}

}
